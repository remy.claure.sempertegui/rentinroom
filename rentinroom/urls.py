"""rentinroom URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/4.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path
from api import views
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = [
    # Requirements : Think of each requirement as an endpoint for the API (a Django view)
    path('admin/', admin.site.urls),
    #- The business can create a room with M capacity'     
    path('api/room/create', views.Room_APIView.as_view()), 

    #- The business can create events for every room. 
    path('api/event/create', views.Event_APIView.as_view()), 

    #- The business can delete a room if said room does not have any events. - A customer can book a place for an event. 
    path('api/room/delete/<pk>', views.Room_APIView.as_view()),     

    #- A customer can book a place for an event. 
    path('api/book/create', views.Book_APIView.as_view()), 

    #- A customer can cancel its booking for an event. 
    path('api/book/cancel/<pk>', views.Room_APIView.as_view()),     

    #- A customer can see all the available public events. 
    path('api/event/list', views.Event_APIView.as_view()), 
    
]+ static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
