from rest_framework import serializers
from api.models import Room, Event, Book
class RoomSerializers(serializers.ModelSerializer):
    class Meta:
        model = Room  
        exclude = []


class EventSerializers(serializers.ModelSerializer):
    class Meta:
        model = Event  
        exclude = []


class BookSerializers(serializers.ModelSerializer):
    class Meta:
        model = Book  
        exclude = []