from django.db import models
from django.contrib.auth.models import User

# Create your models here.
class Room(models.Model):
    capacity = models.IntegerField()  # Consideration: Each room has a different capacity.   
    def __str__(self):
        return '{}'.format(self.id)        


class Event(models.Model):
     name_event = models.CharField(max_length=25)
     room = models.ForeignKey(Room, on_delete=models.CASCADE)
     type_event = models.CharField(max_length=7) #There are two types of events: public and private. 
     date = models.DateField()

     def __str__(self):
        return '{}'.format(self.name_event)        

     def count_avaible(self):
        books = Book.objects.filter(event=self, state="Active")
        if books:
            return self.room.capacity - len(books)
        else:
            return self.room.capacity

     def is_avaible(self):
        return self.count_avaible() > 0
    
     def is_public(self):
        return self.type_event=="Public"


class Book(models.Model):        
    date = models.DateField()
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    event = models.ForeignKey(Event, on_delete=models.CASCADE)
    state = models.CharField(max_length=7) #Choices: Active or cancel 

    def __str__(self):
        return '{}'.format(self.event.name_event)        
