from django.shortcuts import render
from .serializers import RoomSerializers, EventSerializers, BookSerializers
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from .models import Room, Event, Book
from django.http import Http404


# Create your views here.

class Room_APIView(APIView):

    def get(self, request, format=None, *args, **kwargs):
        room = Room.objects.all()
        serializer = RoomSerializers(room, many=True)       
        return Response(serializer.data)

    def post(self, request, format=None):
        """    
        - The business can create a room with M capacity
        """
        serializer = RoomSerializers(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
    
    def delete(self, request, pk, format=None):
        """    
        - The business can delete a room if said room does not have any events. - A customer can book a place for an event. 
        """
        room = Room.objects.get(id=pk)
        events = Event.objects.filter(room=room)
        if not events or len(events)==0:
           room.delete()
           return Response(status=status.HTTP_204_NO_CONTENT)
        else:
             return Response("Error can't delete room because have books", status=status.HTTP_400_BAD_REQUEST)


class Event_APIView(APIView):

    def get(self, request, format=None, *args, **kwargs):
        """
        - A customer can see all the available public events. 
        """
        event = Event.objects.filter(type_event='Public')        
        event1 = []
        for e in event:
            if e.is_avaible():
               event1.append(e)
        serializer = EventSerializers(event1, many=True)       
        return Response(serializer.data)

    def post(self, request, format=None):
        """
        - The business can create events for every room. 
        """
        serializer = EventSerializers(data=request.data)
        events_day = Event.objects.filter(date=request.data['date'])
        if not events_day: #Considerations : For now, there is only one event per day, don't spedify if one event per day for room or general for all.
            if serializer.is_valid():
              serializer.save()
              return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
        else:
            return Response("Only one event for day...!!!", status=status.HTTP_400_BAD_REQUEST)  
        


class Book_APIView(APIView):

    def get(self, request, format=None, *args, **kwargs):
        """
        - A customer can see all the available public events. 
        """
        post = Book.objects.all()
        serializer = BookSerializers(post, many=True)        
        return Response(serializer.data)
    
    def put(self, request, pk, format=None):
        """
        - A customer can cancel its booking for an event. 
        """
        post = self.get_object(pk)
        serializer = BookSerializers(post, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def post(self, request, format=None):
        """    
        - If the event is public, any customer can book a space.
        - If the event is private, no one else can book a space in the room.
        - A customer cannot book a space twice for the same event.
        """

        data1=request.data
        data1["user"]=request.user.id
        data1["state"]="Active"
        events = Event.objects.filter(id=data1["event"]).first()
        if events and events.is_public():
            books_after = len(Book.objects.filter(user_id=request.user.id, event=events)) 
            if books_after==0:
                print(events.count_avaible())
                if events.count_avaible()>0:
                    serializer = BookSerializers(data=request.data)
                    if serializer.is_valid():
                      serializer.save()
                      return Response(serializer.data, status=status.HTTP_201_CREATED)
                else:
                    return Response("The room hasn't enough space!!!", status=status.HTTP_400_BAD_REQUEST) 
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            else:
                 return Response("A customer cannot book a space twice for the same event.", status=status.HTTP_400_BAD_REQUEST) 
        else:
             return Response("The event not is public..!!!", status=status.HTTP_400_BAD_REQUEST) 
    
    def cancel(self, request, format=None):
        """    
        - If the event is public, any customer can book a space.
        - If the event is private, no one else can book a space in the room.
        - A customer cannot book a space twice for the same event.
        """

        data1=request.data
        data1["user"]=request.user.id
        book = Event.objects.filter(id=data1["event"]).first()        
        if book:
            serializer = BookSerializers(data=request.data, isinstance=book)
            if serializer.state=="Active":
              serializer.state="Cancel"
              serializer.save()
              return Response(serializer.data, status=status.HTTP_201_CREATED)
            else: 
                return Response("Don't be Acive the book..!!!", status=status.HTTP_400_BAD_REQUEST) 
        else:
             return Response("Don't exist the book..!!!", status=status.HTTP_400_BAD_REQUEST) 
        