from django.test import TestCase
from rest_framework.test import APIClient
from rest_framework import status
from django.test import TestCase
from api.models import User, Room, Event
import json

# Create your tests here.

class UserTestCase(TestCase):
    def setUp(self):
        user = User(
            email='testing@remyclaure.com',
            first_name='Testing',
            last_name='Testing',
            username='admin'
        )
        user.set_password('admin123')
        user.save()

    def test_create_room(self):
        """Check if create a roon"""
        
        client = APIClient()
        response = client.post(
                '/api/room/create', {
                'capacity': 100,                
            },
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(response.content), {"id":1, "capacity":100})
    
    def test_create_event(self):
        """Check if create a event"""
        room=Room(capacity=100)
        room.save()
        room = Room.objects.all().order_by("-id").first()

        client = APIClient()
        response = client.post(
                '/api/event/create', {
                'name_event': 'Party of Day of dead',
                'type_event': 'Public',
                'date': '2022-11-02',
                'room': room.id
            },
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(json.loads(response.content), {'id':1, 'name_event': 'Party of Day of dead', 'type_event': 'Public', 'date': '2022-11-02', 'room': room.id})

        client = APIClient()
        response = client.post(
                '/api/event/create', {
                'name_event': 'Party birthday',
                'type_event': 'Public',
                'date': '2022-11-02',
                'room': room.id
            },
            format='json'
        )
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json.loads(response.content), 'Only one event for day...!!!')
    
    def test_create_book(self):
        """Check if create a book"""
        room=Room(capacity=100)
        room.save()
        
        event = Event(name_event='Party of Day of dead 2', type_event='Public', date='2022-11-03', room=room)
        event.save()        
        
        client = APIClient()
        response = client.post(
                '/api/book/create', {
                'date': '2022-11-03', 'state': 'Active', 'user': 1, 'event': event.id 
            },
            format='json'
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(json.loads(response.content), {'date': '2022-11-03', 'state': 'Active', 'user': 1, 'event': 1 })

        
    
    