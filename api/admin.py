from django.contrib import admin

# Register your models here.
from .models import Book, Event, Room
admin.site.register(Room)
admin.site.register(Event)
admin.site.register(Book)
